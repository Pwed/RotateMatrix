package main

import (
	"fmt"
)

type matrix [][]int

func main() {
	start := matrix{
		[]int{1, 2, 3, 4},
		[]int{5, 6, 7, 8},
		[]int{9, 0, 1, 2},
		[]int{3, 4, 5, 6},
	}

	fmt.Println(start)

	start.rotateInPlace()

	fmt.Println(start)

	end := start.rotateCopy()

	fmt.Println(end)
}

//rotates a matrix without duplicating the arrays
func (m matrix) rotateInPlace() {
	var sub1, sub2 int
	for row := 0; row < len(m)/2; row++ {
		for col := row; col < len(m)-row-1; col++ {
			sub1 = m[len(m)-1-col][row]
			m[len(m)-1-col][row] = m[row][col]
			sub2 = m[len(m)-1-row][len(m)-1-col]
			m[len(m)-1-row][len(m)-1-col] = sub1
			sub1 = m[col][len(m)-1-row]
			m[col][len(m)-1-row] = sub2
			m[row][col] = sub1
		}
	}
}

// Rotates a matrix by copying each cell to a new matrix
func (m matrix) rotateCopy() matrix {
	m2 := make([][]int, len(m))
	for i := range m2 {
		m2[i] = make([]int, len(m))
	}
	for row := 0; row < len(m)/2; row++ {
		for col := row; col < len(m)-row-1; col++ {
			m2[len(m)-1-col][row] = m[row][col]
			m2[len(m)-1-row][len(m)-1-col] = m[len(m)-1-col][row]
			m2[col][len(m)-1-row] = m[len(m)-1-row][len(m)-1-col]
			m2[row][col] = m[col][len(m)-1-row]
		}
	}
	return m2
}

func (m matrix) String() string {
	var s string
	for row := 0; row < len(m); row++ {
		for cell := 0; cell < len(m); cell++ {
			s = fmt.Sprintf("%s%v", s, m[row][cell])
		}
		s = s + "\n"
	}
	return s
}
